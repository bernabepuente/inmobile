package xoubin.inmobile.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import xoubin.inmobile.dto.TipoInmuebleDto;
import xoubin.inmobile.mapper.TipoInmuebleMapper;
import xoubin.inmobile.model.TipoInmueble;
import xoubin.inmobile.repository.TipoInmuebleRepository;
import xoubin.inmobile.service.TipoInmuebleService;;

@Service("tipo_inmuebleService")
@Transactional
public class TipoInmuebleServiceImpl implements TipoInmuebleService {

	@Autowired
	private TipoInmuebleRepository tipoRepository;
	public List<TipoInmuebleDto> getAllTipo_inmuebles() {
		List<TipoInmueble> listTipos = tipoRepository.findAll();
		if (listTipos.size() > 0) {
			List<TipoInmuebleDto> listTiposDto = new ArrayList<TipoInmuebleDto>(listTipos.size());
			for (TipoInmueble Tipo : listTipos) {
				listTiposDto.add(TipoInmuebleMapper.mapFromEntity(Tipo));
			}
	        return listTiposDto;
		}
		return null;
    }

    public void addTipo_inmueble(TipoInmueble t) {
        if (t != null) {
        	tipoRepository.saveAndFlush(t);
        }
    }

	public TipoInmuebleDto getTipo_inmueble(Long id) {
		TipoInmuebleDto TipoDto = TipoInmuebleMapper.mapFromEntity(tipoRepository.findOne(id));
		return TipoDto;
	}

	public void updateTipo_inmueble(TipoInmueble tipo) {
		if (tipo != null) {
			tipoRepository.saveAndFlush(tipo);
		}
	}

	public void deleteTipo_inmueble(Long tipoId) {
		if (tipoId != null) {
			tipoRepository.delete(Long.parseLong(getTipo_inmueble(tipoId).getId().toString()));
		}
	}
}