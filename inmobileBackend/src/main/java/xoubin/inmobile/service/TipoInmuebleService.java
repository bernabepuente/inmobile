package xoubin.inmobile.service;

import xoubin.inmobile.dto.TipoInmuebleDto;
import xoubin.inmobile.model.TipoInmueble;

import java.util.List;

public interface TipoInmuebleService {
    public void addTipo_inmueble(TipoInmueble tipo_inmueble);
    public List<TipoInmuebleDto> getAllTipo_inmuebles();
    public void deleteTipo_inmueble(Long tipo_inmuebleId);
    public TipoInmuebleDto getTipo_inmueble(Long tipo_inmuebleId);
    public void updateTipo_inmueble(TipoInmueble tipo_inmueble);
}
