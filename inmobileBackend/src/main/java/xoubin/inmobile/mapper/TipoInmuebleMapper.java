package xoubin.inmobile.mapper;

import xoubin.inmobile.model.TipoInmueble;
import xoubin.inmobile.dto.TipoInmuebleDto;

public class TipoInmuebleMapper {
	public static TipoInmuebleDto mapFromEntity(TipoInmueble entity) {
		TipoInmuebleDto Tipo_inmuebleDto = new TipoInmuebleDto();
		Tipo_inmuebleDto.setId(entity.getId());
		Tipo_inmuebleDto.setName(entity.getName());
		return Tipo_inmuebleDto;
	}
}