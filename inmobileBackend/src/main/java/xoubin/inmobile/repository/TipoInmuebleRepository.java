package xoubin.inmobile.repository;

import xoubin.inmobile.model.TipoInmueble;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TipoInmuebleRepository extends JpaRepository<TipoInmueble, Long> {
    TipoInmueble findByName( String name );
}

